﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.ascx.cs" Inherits="uLocate.Web.UserControls.Dashboard" %>

<script src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>
    <%= this.JsonAddresses %>
    google.maps.event.addDomListener(window, 'load', function () {
        var map_canvas = document.getElementById('map-canvas'),
            options = {
                center: new google.maps.LatLng(30, 0),
                zoom: 2,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            map = new google.maps.Map(map_canvas, options),
            bounds = new google.maps.LatLngBounds(),
            infowindow = new google.maps.InfoWindow({
                content: '<div>Loading...</div>'
            });

        if (addresses != null && addresses.length > 0) {
            for (var i = 0; i < addresses.length; i++) {
                var coords = new google.maps.LatLng(addresses[i].Coordinate.latitude, addresses[i].Coordinate.longitude),
                    marker = new google.maps.Marker({
                        map: map,
                        position: coords,
                        html: '<strong>' + addresses[i].Name + '</strong><br/>'
                            + (addresses[i].Address1 != '' ? addresses[i].Address1 + '<br/>' : '')
                            + (addresses[i].Address2 != '' ? addresses[i].Address2 + '<br/>' : '')
                            + (addresses[i].Locality != '' ? addresses[i].Locality + '<br/>' : '')
                            + (addresses[i].Region != '' ? addresses[i].Region + '<br/>' : '')
                            + (addresses[i].PostalCode != '' ? addresses[i].PostalCode + '<br/>' : '')
                            + (addresses[i].CountryCode != '' ? addresses[i].CountryCode + '<br/>' : '')
                            + '<br/><a href="plugins/uLocate/EditGeocodedAddress.aspx?id=' + addresses[i].Id + '">Edit address</a>'
                    });

                google.maps.event.addListener(marker, 'click', function () {
                    var div = document.createElement('div');
                    div.innerHTML = this.html;
                    infowindow.setContent(div);
                    infowindow.open(map, this);
                });

                bounds.extend(coords);
                map.fitBounds(bounds);
            }
        }

        $(map_canvas).css('height', window.innerHeight - 125);
    });
</script>

<div id="map-canvas"></div>
