﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using uLocate.Core;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.packager.standardPackageActions;
using umbraco.interfaces;
using Umbraco.Core;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;



namespace uLocate.Web.PackageActions
{
    /// <summary>
    /// This package action will add a new nodeType to the `UI.xml` file for additional TreeNode tasks of Umbraco backend trees.
    /// </summary>
    /// <remarks>
    /// This package action has been customized from the PackageActionsContrib Project.
    /// http://packageactioncontrib.codeplex.com
    /// </remarks>
    public class AddTreeNodeType : IPackageAction
    {
        private const string UmbracoUIPath = "~/umbraco/config/create/UI.xml";

        /// <summary>
        /// This Alias must be unique and is used as an identifier that must match the alias in the package action XML.
        /// </summary>
        /// <returns>The Alias of the package action.</returns>
        public string Alias()
        {
            return string.Concat(uLocate.Core.Constants.ApplicationName, "_AddTreeNodeType");
        }

        /// <summary>
        /// Executes the specified package name.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="xmlData">The XML data.</param>
        /// <returns></returns>
        public bool Execute(string packageName, XmlNode xmlData)
        {
            var newNodes = xmlData.SelectNodes("//nodeType");

            if (newNodes.Count == 0)
                return false;

            var document = XmlHelper.OpenAsXmlDocument(UmbracoUIPath);
            var rootNode = document.SelectSingleNode("//createUI");

            if (rootNode == null)
                return false;

            var modified = false;

            foreach (XmlNode newNode in newNodes)
            {
                var insertNode = true;

                if (rootNode.HasChildNodes)
                {
                    string alias = newNode.Attributes["alias"].Value;

                    var node = rootNode.SelectSingleNode(string.Format("//nodeType[@alias = '{0}']", alias));

                    if (node != null)
                    {
                        insertNode = false;
                    }
                }

                if (insertNode)
                {
                    rootNode.AppendChild(document.ImportNode(newNode, true));

                    modified = true;
                }
            }

            if (modified)
            {
                document.Normalize();

                try
                {
                    document.Save(IOHelper.MapPath(UmbracoUIPath));

                    return true;
                }
                catch (Exception ex)
                {
                    var message = string.Concat("Error at install ", this.Alias(), " package action: ", ex);
                    LogHelper.Error(typeof(AddLanguageFileKey), message, ex);
                }
            }

            return false;
        }

        /// <summary>
        /// Returns a Sample XML Node.
        /// </summary>
        /// <returns>The sample XML as node.</returns>
        public XmlNode SampleXml()
        {
            return helper.parseStringToXmlNode(string.Concat(
            @"<Action runat=""install"" undo=""true"" alias=""", this.Alias(), @""">
                <nodeType alias=""ulocateGeocodedAddress"">
                    <header>Geocoded Address</header>
                    <usercontrol>/create/simple.ascx</usercontrol>
                    <tasks>
                        <create assembly=""uLocate.Web"" type=""GeocodedAddressTasks"" />
                        <delete assembly=""uLocate.Web"" type=""GeocodedAddressTasks"" />
                    </tasks>
                </nodeType>
            </Action>"));
        }

        /// <summary>
        /// Undoes the specified package name.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="xmlData">The XML data.</param>
        /// <returns></returns>
        public bool Undo(string packageName, XmlNode xmlData)
        {
            var undoNodes = xmlData.SelectNodes("//nodeType");

            if (undoNodes.Count == 0)
                return false;

            var document = XmlHelper.OpenAsXmlDocument(UmbracoUIPath);
            var rootNode = document.SelectSingleNode("//createUI");

            if (rootNode == null)
                return false;

            var modified = false;

            foreach (XmlNode undoNode in undoNodes)
            {
                if (rootNode.HasChildNodes)
                {
                    var alias = undoNode.Attributes["alias"].Value;

                    foreach (XmlNode existingNode in rootNode.SelectNodes(string.Format("//nodeType[@alias = '{0}']", alias)))
                    {
                        rootNode.RemoveChild(existingNode);
                        modified = true;
                    }
                }
            }

            if (modified)
            {
                document.Normalize();

                try
                {
                    document.Save(IOHelper.MapPath(UmbracoUIPath));

                    return true;
                }
                catch (Exception ex)
                {
                    var message = string.Concat("Error at undo ", this.Alias(), " package action: ", ex);
                    LogHelper.Error(typeof(AddLanguageFileKey), message, ex);
                }
            }

            return false;
        }
    }
}