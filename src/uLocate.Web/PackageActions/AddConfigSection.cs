﻿using System;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using uLocate.Core;
using uLocate.Core.Configuration;
using umbraco.cms.businesslogic.packager.standardPackageActions;
using umbraco.interfaces;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;

namespace uLocate.Web.PackageActions
{
    /// <summary>
    /// This package action will Add a new HTTP Module to the web.config file.
    /// </summary>
    /// <remarks>
    /// This package action has been customized from the PackageActionsContrib Project.
    /// http://packageactioncontrib.codeplex.com
    /// </remarks>
    public class AddConfigSection : IPackageAction
    {
        /// <summary>
        /// This Alias must be unique and is used as an identifier that must match the alias in the package action XML.
        /// </summary>
        /// <returns>The Alias of the package action.</returns>
        public string Alias()
        {
            return string.Concat(Constants.ApplicationName, "_AddConfigSection");
        }

        /// <summary>
        /// Executes the specified package name.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="xmlData">The XML data.</param>
        /// <returns></returns>
        public bool Execute(string packageName, XmlNode xmlData)
        {
            try
            {
                var webConfig = WebConfigurationManager.OpenWebConfiguration("~/");
                if (webConfig.Sections[Constants.ApplicationName] == null)
                {
                    webConfig.Sections.Add(Constants.ApplicationName, new uLocateSection());

                    var configPath = string.Concat("config", Path.DirectorySeparatorChar, Constants.ApplicationName, ".config");
                    var xmlPath = IOHelper.MapPath(string.Concat("~/", configPath));
                    string xml;

                    using (var reader = new StreamReader(xmlPath))
                    {
                        xml = reader.ReadToEnd();
                    }

                    webConfig.Sections[Constants.ApplicationName].SectionInformation.SetRawXml(xml);
                    webConfig.Sections[Constants.ApplicationName].SectionInformation.ConfigSource = configPath;

                    webConfig.Save(ConfigurationSaveMode.Modified);
                }

                return true;
            }
            catch (Exception ex)
            {
                var message = string.Concat("Error at install ", this.Alias(), " package action: ", ex);
                LogHelper.Error(typeof(AddConfigSection), message, ex);
            }

            return false;
        }

        /// <summary>
        /// Returns a Sample XML Node
        /// </summary>
        /// <returns>The sample xml as node</returns>
        public XmlNode SampleXml()
        {
            var xml = string.Concat("<Action runat=\"install\" undo=\"true\" alias=\"", this.Alias(), "\" />");
            return helper.parseStringToXmlNode(xml);
        }

        /// <summary>
        /// Undoes the specified package name.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="xmlData">The XML data.</param>
        /// <returns></returns>
        public bool Undo(string packageName, XmlNode xmlData)
        {
            try
            {
                var webConfig = WebConfigurationManager.OpenWebConfiguration("~/");
                if (webConfig.Sections[Constants.ApplicationName] != null)
                {
                    webConfig.Sections.Remove(Constants.ApplicationName);

                    webConfig.Save(ConfigurationSaveMode.Modified);
                }

                return true;
            }
            catch (Exception ex)
            {
                var message = string.Concat("Error at undo ", this.Alias(), " package action: ", ex);
                LogHelper.Error(typeof(AddConfigSection), message, ex);
            }

            return false;
        }
    }
}