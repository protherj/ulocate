﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditGeocodedAddress.aspx.cs" masterpagefile="~/Umbraco/masterpages/umbracoPage.Master" Inherits="uLocate.Web.WebPages.EditGeocodedAddress" %>
<%@ register namespace="umbraco.uicontrols" assembly="controls" tagprefix="umb" %>

 <asp:content id="head" runat="server" contentplaceholderid="head">
     <style>

         .map
         {
             height: 300px;
             margin-top: 20px;
             border: 1px solid #d9d7d7;
         }
     </style>
     <script src="bgGoogle.js"></script>
     <script>
         $(document).ready(function () {
             bg.Google.map.load('.map', { center: $('.map .geo'), marker: true });
         });
     </script>
 </asp:content>

<asp:content id="content" contentplaceholderid="body" runat="server">

    <umb:umbracopanel id="uPanel" runat="server" hasmenu="true" text="Geocoded Address">

        <umb:pane id="pane" runat="server">

            <umb:propertypanel id="ppanel" runat="server" text="Name">
                <asp:textbox id="txtName" runat="server" cssclass="guiInputText guiInputStandardSize" /> <asp:requiredfieldvalidator id="rfvTxtName" runat="server" controltovalidate="txtName" display="Dynamic" errormessage=" *required" />
            </umb:propertypanel>
                 
            <umb:propertypanel id="ppAddress1" runat="server" text="Address 1">
                <asp:textbox id="txtAddress1" runat="server" maxlength="255" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppAddress2" runat="server" text="Address 2">
                <asp:textbox id="txtAddress2" runat="server"  maxlength="255" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppLocality" runat="server" text="Locality">
                <asp:textbox id="txtLocality" runat="server"  maxlength="50" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppRegion" runat="server" text="Region">
                <asp:textbox id="txtRegion" runat="server"   maxlength="50" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppPostalCode" runat="server" text="Postal code">
                <asp:textbox id="txtPostalCode" runat="server"   maxlength="50" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>
            
            <umb:propertypanel id="ppCountryCode" runat="server" text="Country Code">
                <asp:dropdownlist id="ddlCountryCode" runat="server" cssclass="guiInputText guiInputStandardSize"  />
            </umb:propertypanel>

    </umb:pane>
        <umb:pane runat="server" id="extended">
            

            
            <umb:propertypanel id="ppTelephone" runat="server" text="Telephone">
                <asp:textbox id="txtTelephone" runat="server" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppFax" runat="server" text="Fax">
                <asp:textbox id="txtFax" runat="server" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppEmail" runat="server" text="Email">
                <asp:textbox id="txtEmail" runat="server" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppWebsiteUrl" runat="server" text="Website Url">
                <asp:textbox id="txtWebsiteUrl" runat="server" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

            <umb:propertypanel id="ppComment" runat="server" text="Comment">
                <asp:textbox id="txtComment" runat="server" textmode="MultiLine" maxlength="500" cssclass="guiInputText guiInputStandardSize" />
            </umb:propertypanel>

          </umb:pane>

        <div id="map" runat="server" class="map">
        <span class="geo">center: [ <span class="latitude"><asp:literal id="litLat" runat="server" /></span>, <span class="longitude"><asp:literal id="litLong" runat="server" /></span> ]</span>

        </div>

        </umb:umbracopanel>



</asp:content>