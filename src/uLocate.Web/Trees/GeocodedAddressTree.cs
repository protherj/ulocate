﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using uLocate.Core;
using umbraco.businesslogic;
using umbraco.BusinessLogic.Actions;
using umbraco.cms.presentation.Trees;
using umbraco.interfaces;

namespace uLocate.Web.BackOffice.Trees
{
    [Tree("ulocate", "ulocateGeocodedAddress", "Geocoded Addresses", "geoAddress.png", "geoAddress.png", sortOrder: 1)]
    public class GeocodedAddressTree : BaseTree
    {
        public GeocodedAddressTree(string application)
            : base(application) { }

        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.NodeType = TreeAlias;
            rootNode.NodeID = "init";
        }

        public override void RenderJS(ref StringBuilder Javascript)
        {
            Javascript.Append(
                @"
                    function viewGeocodedAddresses(letter) {
                        UmbClientMgr.contentFrame('plugins/uLocate/ViewGeocodedAddresses.aspx?letter=' + letter);
                    }

                    function openGeocodedAddress(id) {
                        UmbClientMgr.contentFrame('plugins/uLocate/EditGeocodedAddress.aspx?id=' + id);
                    }

                ");
        }

        public override void Render(ref XmlTree Tree)
        {
            var addresses = ULocateHelper.GeocodedAddressList();
            var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var letter = string.Empty;

            if (HttpContext.Current.Request.QueryString.ToString().IndexOf("letter") >= 0)
            {
                letter = HttpContext.Current.Request.QueryString.Get("letter");
            }

            if (!string.IsNullOrEmpty(letter))
            {
                // determine if this is an alpha folder or the other folder
                var addressSet = letter.ToLower() == "other" ?
                    addresses.Where(n => !Char.IsLetter(n.Name[0])).OrderBy(n => n.Name) :
                    addresses.Where(n => n.Name.StartsWith(letter, StringComparison.InvariantCultureIgnoreCase)).OrderBy(n => n.Name);

                foreach (var address in addressSet)
                {
                    var treeNode = XmlTreeNode.Create(this);
                    treeNode.Menu = new List<IAction>() { ActionDelete.Instance };
                    treeNode.NodeID = address.Id.ToString();
                    treeNode.Text = HttpUtility.JavaScriptStringEncode(address.Name);
                    treeNode.Action = string.Format("javascript:openGeocodedAddress({0});", address.Id);
                    treeNode.Source = string.Empty;
                    treeNode.Icon = "doc.gif";
                    treeNode.OpenIcon = "doc.gif";
                    treeNode.NodeType = "ulocateGeocodedAddress";
                    treeNode.HasChildren = false;

                    Tree.Add(treeNode);
                }
            }
            else
            {
                // create the alpha folders
                foreach (var c in alpha.ToArray())
                {
                    if (addresses.Where(n => n.Name.StartsWith(c.ToString(), StringComparison.InvariantCultureIgnoreCase)).Count() > 0)
                    {
                        var treeElement = XmlTreeNode.Create(this);
                        treeElement.Menu = new List<IAction>() { ActionRefresh.Instance };
                        treeElement.NodeID = c.ToString();
                        treeElement.Text = c.ToString();
                        treeElement.Action = string.Format("javascript:viewGeocodedAddresses('{0}');", c);
                        treeElement.Source = string.Format("tree.aspx?letter={0}&app={1}&treeType={2}&rnd={3}", c, m_app, HttpContext.Current.Request.QueryString["treeType"], Guid.NewGuid());
                        treeElement.Icon = FolderIcon;
                        treeElement.OpenIcon = FolderIcon;
                        treeElement.NodeType = "ulocate";
                        treeElement.HasChildren = true;

                        Tree.Add(treeElement);
                    }
                }

                // we need an "other" folder to store names that do not begin with an alpha character  
                var others = addresses.Where(n => !Char.IsLetter(n.Name[0]));

                if (others.Count() > 0)
                {
                    var treeElement = XmlTreeNode.Create(this);
                    treeElement.Menu = new List<IAction>() { ActionRefresh.Instance };
                    treeElement.NodeID = "other";
                    treeElement.Text = "Other";
                    treeElement.Action = "javascript:viewGeocodedAddresses('other');";
                    treeElement.Source = string.Format("tree.aspx?letter=other&app={0}&treeType={1}&rnd={2}", m_app, HttpContext.Current.Request.QueryString["treeType"], Guid.NewGuid());
                    treeElement.Icon = FolderIcon;
                    treeElement.OpenIcon = FolderIcon;
                    treeElement.NodeType = "ulocate";
                    treeElement.HasChildren = true;

                    Tree.Add(treeElement);
                }
            }
        }
    }
}