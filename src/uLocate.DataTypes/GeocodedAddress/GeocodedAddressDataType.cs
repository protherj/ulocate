﻿using System;
using uLocate.Core;
using umbraco.cms.businesslogic.datatype;
using umbraco.interfaces;

namespace uLocate.DataTypes.GeocodedAddress
{
    /// <summary>
    /// Data Editor for the GeocodedAddress data-type.
    /// </summary>
    public class GeocodedAddressDataType : AbstractDataEditor
    {
        /// <summary>
        /// The control for the GeocodedAddress data-editor.
        /// </summary>
        private GeocodedAddressControl m_Control = new GeocodedAddressControl();

        /// <summary>
        /// The PreValue Editor for the data-type.
        /// </summary>
        private IDataPrevalue m_PreValueEditor;

        /// <summary>
        /// Initializes a new instance of the <see cref="GeocodedAddressDataType"/> class.
        /// </summary>
        public GeocodedAddressDataType()
        {
            // set the render control as the placeholder
            this.RenderControl = this.m_Control;

            // assign the initialise event for the control
            this.m_Control.Init += new EventHandler(this.m_Control_Init);
            this.m_Control.Load += new EventHandler(this.m_Control_Load);

            // assign the save event for the data-type/editor
            this.DataEditorControl.OnSave += new AbstractDataEditorControl.SaveEventHandler(this.DataEditorControl_OnSave);
        }

        /// <summary>
        /// Gets the id of the data-type.
        /// </summary>
        /// <value>The id of the data-type.</value>
        public override Guid Id
        {
            get
            {
                return new Guid(DataTypeConstants.GeocodedAddressId);
            }
        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>The name of the data type.</value>
        public override string DataTypeName
        {
            get
            {
                return string.Concat(Constants.ApplicationName, ": GeocodedAddress");
            }
        }

        /// <summary>
        /// Gets the prevalue editor.
        /// </summary>
        /// <value>The prevalue editor.</value>
        public override IDataPrevalue PrevalueEditor
        {
            get
            {
                if (this.m_PreValueEditor == null)
                {
                    this.m_PreValueEditor = new GeocodedAddressPrevalueEditor(this);
                }

                return this.m_PreValueEditor;
            }
        }

        /// <summary>
        /// Handles the Init event of the m_Control control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void m_Control_Init(object sender, EventArgs e)
        {
            var options = ((GeocodedAddressPrevalueEditor)this.PrevalueEditor).GetPreValueOptions<GeocodedAddressOptions>();

            // set the controls options
            this.m_Control.Options = options;
        }

        /// <summary>
        /// Handles the Init event of the control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void m_Control_Load(object sender, EventArgs e)
        {
            var addressId = -1;
            // check if the data value is available...
            if (this.Data.Value != null && int.TryParse(this.Data.Value.ToString(), out addressId))
            {
                // set the value of the control
                this.m_Control.AddressId = addressId;
            }
        }

        /// <summary>
        /// Saves the data for the editor control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DataEditorControl_OnSave(EventArgs e)
        {
            this.Data.Value = this.m_Control.Save();
        }
    }
}
