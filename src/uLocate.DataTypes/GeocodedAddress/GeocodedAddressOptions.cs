﻿using System.ComponentModel;
using umbraco.editorControls;

namespace uLocate.DataTypes.GeocodedAddress
{
    /// <summary>
    /// The options for the GeocodedAddress data-type.
    /// </summary>
    public class GeocodedAddressOptions : AbstractOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeocodedAddressOptions"/> class.
        /// </summary>
        public GeocodedAddressOptions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GeocodedAddressOptions"/> class.
        /// </summary>
        /// <param name="loadDefaults">if set to <c>true</c> [load defaults].</param>
        public GeocodedAddressOptions(bool loadDefaults)
            : base(loadDefaults)
        {
        }

        [DefaultValue(true)]
        public bool ShowNameField { get; set; }
    }
}