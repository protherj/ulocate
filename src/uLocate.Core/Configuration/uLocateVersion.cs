﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace uLocate.Core.Configuration
{
    public class uLocateVersion
    {
        private static readonly Version Version = new Version("2.0.0.0");

        /// <summary>
        /// Gets the current version of uLocate.
        /// Version class with the specified major, minor, build (Patch), and revision numbers.
        /// </summary>
        /// <remarks>
        /// CURRENT ULOCATE VERSION ID.
        /// </remarks>
        public static Version Current
        {
            get { return Version; }
        }

        /// <summary>
        /// Gets the version comment (like beta or RC).
        /// </summary>
        /// <value>The version comment.</value>
        public static string CurrentComment { get { return "0"; } }

        // Get the version of the umbraco.dll by looking at a class in that dll
        // Had to do it like this due to medium trust issues, see: http://haacked.com/archive/2010/11/04/assembly-location-and-medium-trust.aspx
        public static string AssemblyVersion { get { return new AssemblyName(typeof(uLocateVersion).Assembly.FullName).Version.ToString(); } }
    }
}
