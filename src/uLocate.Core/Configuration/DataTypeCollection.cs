﻿using System.Configuration;

namespace uLocate.Core.Configuration
{
    public class DataTypeCollection : ConfigurationElementCollection
    {

        /// <summary>
        /// Creates a new <see cref="ConfigurationElement">ConfigurationElement</see>.
        /// CreateNewElement must be overridden in classes that derive from the ConfigurationElementCollection class.
        /// </summary>
        protected override ConfigurationElement CreateNewElement()
        {
            return new DataTypeElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">DataTypeElement</param>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DataTypeElement)element).Name;
        }

        /// <summary>
        /// Default. Returns the DataTypeElement with the index of index from the collection
        /// </summary>
        protected DataTypeElement this[object index]
        {
            get { return (DataTypeElement)this.BaseGet(index); }
        }
    }
}
