﻿using System;
using System.Collections.Generic;
using System.Web;
using uLocate.Core.Models;

namespace uLocate.Core.Geocoding
{
    public abstract class AbstractGeocodeServiceProvider : AbstractCacheable
    {
        abstract public IGeocodeResponse GetGeocodeResponse(string addressString);

        /// <summary>
        /// Returns a Geocode response from 
        /// </summary>
        /// <param name="address">The street address value</param>
        /// <param name="locality">The locality or city value</param>
        /// <param name="region">The region or state value</param>
        /// <param name="postalCode">The postal code value</param>
        /// <returns></returns>
        abstract public IGeocodeResponse GetGeocodeResponse(string address1, string address2, string locality, string region, string postalCode, string countryCode);

        /// <summary>
        /// Enable cache setting for the GeocodeServiceProvider
        /// </summary>
        protected static bool EnableCaching { get; set; }

        /// <summary>
        /// Cache duration value for the GeocodeServiceProvider - value assumed to be in seconds
        /// </summary>
        protected static int CacheDuration { get; set; }


        /// <summary>
        /// True/false to indicate whether or not geocode requests should be logged.
        /// </summary>
        protected static bool LogRequests { get; set; }

        /// <summary>
        /// Additionnal configurations found in the configuration file
        /// </summary>
        protected IDictionary<string, string> Settings { get; set; }

        /// <summary>
        /// Adds object data to cache if cache is enabled
        /// </summary>
        /// <param name="key">Cache key</param>
        /// <param name="data">Object data to be cached</param>
        protected static void CacheData(string key, object data)
        {
            if (EnableCaching && data != null)
            {
                HttpContext.Current.Cache.Insert(key, data, null, DateTime.Now.AddSeconds(CacheDuration), TimeSpan.Zero);
            }
        }
        

        /// <summary>
        /// Instance of the GeocodeServiceProvider
        /// </summary>
        private static AbstractGeocodeServiceProvider m_Instance;
        internal static AbstractGeocodeServiceProvider Instance()
        {
            if (m_Instance == null)
            {                
                m_Instance = (AbstractGeocodeServiceProvider)Activator.CreateInstance(Type.GetType(Constants.Configuration.Providers["GeocodingAPIProvider"].ProviderType));
            }
            return m_Instance;
        }
    }
}