﻿
namespace uLocate.Core.Models
{
    /// <summary>
    /// Marker interface for api geocode responses
    /// </summary>
    public interface IGeocode : ICoordinate
    {
        /// <summary>
        /// The formatted address returned from the API
        /// </summary>
        string FormattedAddress { get; }

        /// <summary>
        /// Quality of the geocode response returned from the API
        /// </summary>
        GeocodeQuality Quality { get; }

        /// <summary>
        /// Recommended bounding box to center and zoom for this result
        /// </summary>
        /// <remarks>
        /// Yahoo! PlaceFinder requires the 'X' flag to be set to return the boundingbox
        /// http://developer.yahoo.com/geo/placefinder/guide/requests.html#flags-parameter
        /// </remarks>
        Viewport Viewport { get; }
    }


}
