﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uLocate.Core.Models
{
    public interface IAddress
    {
        /// <summary>
        /// The Name of the address
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Address line one
        /// </summary>
        string Address1 { get; set; }

        /// <summary>
        /// Address line two
        /// </summary>
        string Address2 { get; set; }

        /// <summary>
        /// The locality (City) of the address
        /// </summary>
        string Locality { get; set; }

        /// <summary>
        /// The region (province/state) of the address
        /// </summary>
        string Region { get; set; }

        /// <summary>
        /// The postal code of the address
        /// </summary>
        string PostalCode { get; set; }

        /// <summary>
        /// The two charactor country code
        /// </summary>
        string CountryCode { get; set; }

    }
}
