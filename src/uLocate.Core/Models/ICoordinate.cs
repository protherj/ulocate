﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uLocate.Core.Models
{
    /// <summary>
    /// Marker interface for coordinates
    /// </summary>
    public interface ICoordinate
    {
        double Latitude { get; }   // y
        double Longitude { get; }  // x
    }
}
