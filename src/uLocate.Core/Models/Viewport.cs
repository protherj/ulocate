﻿using System;
using System.Runtime.Serialization;
using Microsoft.SqlServer.Types;
using uLocate.Core.Extensions;

namespace uLocate.Core.Models
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class Viewport
    {
        public Viewport(Coordinate southWest, Coordinate northEast)
        {
            var builder = new SqlGeographyBuilder();
            builder.SetSrid(Constants.SRID_WorldGeodeticSystem);
            builder.BeginGeography(OpenGisGeographyType.LineString);
            builder.BeginFigure(northEast.Latitude, northEast.Longitude);
            builder.AddLine(southWest.Latitude, southWest.Longitude);
            builder.EndFigure();
            builder.EndGeography();

            SqlGeography = builder.ConstructedGeography;
        }

        public Viewport(SqlGeography sqlGeography)
        {
            if (sqlGeography.InstanceOf("LineString").IsTrue)
            {
                SqlGeography = sqlGeography;
            }
            else
            {
                throw new Exception("SqlGeography used to instantiate the Viewport was not of type LineString");
            }
        }

        public SqlGeography SqlGeography { get; private set; }

        /// <summary>
        /// The North East coordinate of the suggested viewport
        /// </summary>
        [DataMember(Name = "northeast")]
        public Coordinate NorthEast
        {
            get { return SqlGeography.STStartPoint().ToCoordinate(); }
            private set { }
        }

        /// <summary>
        /// The South West coordinate of the suggested viewport
        /// </summary>
        [DataMember(Name = "southwest")]
        public Coordinate SouthWest
        {
            get { return SqlGeography.STEndPoint().ToCoordinate(); }
            private set { }
        }
    }
}
